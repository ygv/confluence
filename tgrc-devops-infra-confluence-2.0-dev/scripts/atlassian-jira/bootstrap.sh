#!/bin/bash

set -e -o pipefail
set -x
echo `pwd`
source /var/lib/cloud/instance/scripts/part-001
echo "App name is:"$APP_NAME
echo "Env name is:"$ENV

yum install -y unzip
useradd -u 2001 jira
#chmod 777 -R /jira/share
#chown -R jira:jira /jira/share

#testing userdata change
yum install -y unzip


curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install

# required settings
NODE_NAME_PREFIX="chef-client-${APP_NAME}-${ENV}" # prefix the node name with the role of the node, e.g. webserver or rails-app-server
 
NODE_NAME="${NODE_NAME_PREFIX}$(curl --silent --show-error --retry 3 http://169.254.169.254/latest/meta-data/instance-id)" # this uses the EC2 instance ID as the node name
CHEF_SERVER_NAME="Chef-Server-Shared" # The name of your Chef Server
CHEF_SERVER_ENDPOINT="chef-server-shared-9dclfxiyupdny5db.us-east-1.opsworks-cm.io" # The FQDN of your Chef Server
REGION="us-east-1" # Region of your Chef Server (Choose one of our supported regions - us-east-1, us-east-2, us-west-1, us-west-2, eu-central-1, eu-west-1, ap-northeast-1, ap-southeast-1, ap-southeast-2)
ROOT_CA_URL="https://opsworks-cm-${REGION}-prod-default-assets.s3.amazonaws.com/misc/opsworks-cm-ca-2016-root.pem"
CHEF_CLIENT_VERSION="15.0.293" # latest if empty
CHEF_CLIENT_LOG_LOCATION="/var/log/chef-client.log"
CHEF_CLIENT_OPTS="-L ${CHEF_CLIENT_LOG_LOCATION}"
 
# optional settings
CHEF_ORGANIZATION="default" # AWS OpsWorks for Chef Server always creates the organization "default"
NODE_ENVIRONMENT="${APP_NAME}-${ENV}-env" # E.g. development, staging, onebox, ...
RUN_LIST="chef-client,role[${APP_NAME}-role]" # optional, only when not using Policy
JSON_ATTRIBUTES="" # optional, path to a json file for your node object


echo "NODE_NAME_PREFIX:"$NODE_NAME_PREFIX
echo "NODE_ENVIRONMENT:"$NODE_ENVIRONMENT
echo "RUN_LIST:"$RUN_LIST



# extra optional settings
AWS_CLI_EXTRA_OPTS=()
CFN_SIGNAL=""
 
# required settings to define your node configuration
 
# In the example of our Starterkit we are using a policy for your node defined in Policyfile.rb
# To follow our example from the README.md leave RUN_LIST empty
 
mkdir -p "/etc/chef"
 
# when run-list is empty we will use json attributes according to our Policyfile.rb
if [ -z $RUN_LIST ]; then
  (cat <<-JSON
    {
      "name": "${NODE_NAME}",
      "chef_environment": "${NODE_ENVIRONMENT}",
      "policy_name": "opsworks-demo-webserver",
      "policy_group": "opsworks-demo"
    }
JSON
) | sed 's/: ""/: null/g' > /etc/chef/client-attributes.json
fi
 
# ---------------------------
 
AWS_CLI_TMP_FOLDER=$(mktemp --directory "/tmp/awscli_XXXX")
CHEF_CA_PATH="/etc/chef/opsworks-cm-ca-2016-root.pem"
 
prepare_os_packages() {
  local OS=`uname -a`
  if [[ ${OS} = *"Linux"* ]]; then
    yum -y update
    yum -y install unzip python
    yum -y install epel-release
    yum -y install python-pip
    # see: https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/cfn-helper-scripts-reference.html
    pip install https://s3.amazonaws.com/cloudformation-examples/aws-cfn-bootstrap-latest.tar.gz
    ln -s /root/aws-cfn-bootstrap-latest/init/ubuntu/cfn-hup /etc/init.d/cfn-hup
    mkdir -p /opt/aws
    ln -s /usr/local/bin /opt/aws/bin
  fi
}
 
install_aws_cli() {
  # see: http://docs.aws.amazon.com/cli/latest/userguide/installing.html#install-bundle-other-os
  pushd "${AWS_CLI_TMP_FOLDER}"
  curl https://s3.amazonaws.com/aws-cli/awscli-bundle-1.18.200.zip -o awscli-bundle.zip
  unzip "awscli-bundle.zip"
  ./awscli-bundle/install -i "${PWD}"
}
 
aws_cli() {
  "${AWS_CLI_TMP_FOLDER}/bin/aws" opsworks-cm \
    --region "${REGION}" ${AWS_CLI_EXTRA_OPTS[@]:-} --output text "$@" --server-name "${CHEF_SERVER_NAME}"
}
 
set_cli_role() {
  yum install -y jq
  ASSUMED_ROLE=$("${AWS_CLI_TMP_FOLDER}/bin/aws" sts assume-role --role-arn arn:aws:iam::915180751011:role/OWCA-AllowAssociate-Role --role-session-name owca --no-verify-ssl)
  export AWS_ACCESS_KEY_ID=$(echo $ASSUMED_ROLE | jq .Credentials.AccessKeyId | xargs)
  export AWS_SECRET_ACCESS_KEY=$(echo $ASSUMED_ROLE | jq .Credentials.SecretAccessKey | xargs)
  export AWS_SESSION_TOKEN=$(echo $ASSUMED_ROLE | jq .Credentials.SessionToken | xargs)
}
 
 
cleanup_cli_role(){
  yum remove -y jq
  unset AWS_SESSION_TOKEN
  unset AWS_ACCESS_KEY_ID
  unset AWS_SECRET_ACCESS_KEY
}
 
 
associate_node() {
  local client_key="/etc/chef/client.pem"
  mkdir -p /etc/chef
  ( umask 077; openssl genrsa -out "${client_key}" 2048 )
 
  aws_cli associate-node \
    --node-name "${NODE_NAME}" \
    --engine-attributes \
      "Name=CHEF_AUTOMATE_ORGANIZATION,Value=${CHEF_ORGANIZATION}" \
      "Name=CHEF_AUTOMATE_NODE_PUBLIC_KEY,Value='$(openssl rsa -in "${client_key}" -pubout)'"
}
 
write_chef_config() {
  (
    echo "chef_server_url   'https://${CHEF_SERVER_ENDPOINT}/organizations/${CHEF_ORGANIZATION}'"
    echo "node_name         '${NODE_NAME}'"
    echo "ssl_ca_file       '${CHEF_CA_PATH}'"
    echo "chef_license      'accept'"
  ) >> /etc/chef/client.rb
}
 
install_chef_client() {
  # see: https://docs.chef.io/install_omnibus.html
  curl --silent --show-error --retry 3 --location https://omnitruck.chef.io/install.sh | bash -s -- -v "${CHEF_CLIENT_VERSION}"
}
 
install_trusted_certs() {
  curl --silent --show-error --retry 3 --location --output "${CHEF_CA_PATH}" ${ROOT_CA_URL}
}
 
wait_node_associated() {
  aws_cli wait node-associated --node-association-status-token "$1"
}
 
# order of execution of functions
prepare_os_packages
install_aws_cli
set_cli_role
install_chef_client
node_association_status_token="$(associate_node)"
write_chef_config
install_trusted_certs
wait_node_associated "${node_association_status_token}"
cleanup_cli_role
 
# initial chef-client run to register node
 
add_node_environment_to_client_opts() {
  if [ ! -z "${NODE_ENVIRONMENT}" ]; then
    CHEF_CLIENT_OPTS+=("-E ${NODE_ENVIRONMENT}");
  fi
}
 
add_json_attributes_to_client_opts() {
  if [ ! -z "${JSON_ATTRIBUTES}" ]; then
    echo "${JSON_ATTRIBUTES}" > /tmp/chef-attributes.json
    CHEF_CLIENT_OPTS+=("-j /tmp/chef-attributes.json")
  fi
}
 
 
# when the run-list is provided we use this for the chef-client run
if [ ! -z "${RUN_LIST}" ]; then
  # use a regular run_list to run chef
  CHEF_CLIENT_OPTS=(-r "${RUN_LIST}")
  add_node_environment_to_client_opts
  add_json_attributes_to_client_opts
else
  # use a node policy following the example of your Starterkit
  CHEF_CLIENT_OPTS=("-j /etc/chef/client-attributes.json")
fi
 
# initial chef-client run to register node
if [ ! -z "${CHEF_CLIENT_OPTS}" ]; then
  chef-client ${CHEF_CLIENT_OPTS[@]}
fi
 
touch /tmp/userdata.done
eval ${CFN_SIGNAL}