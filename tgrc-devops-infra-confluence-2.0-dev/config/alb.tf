# ALB
# resource "aws_lb" "alb" {
#   name                       = "alb-ue1-${var.tag}-${var.app}-${var.env}"
#   subnets                    = var.subnets
#   security_groups            = [aws_security_group.alb_sg.id, data.aws_security_group.cargill_managed_sg.id]
#   internal                   = false
#   load_balancer_type         = "application"
#   enable_deletion_protection = false
#   tags                       = merge(local.tags, map("Name", "alb-ue1-${var.tag}-${var.app}-${var.env}"))
#   access_logs {
#     bucket  = "${var.team}-alb-log-${var.env}"
#     prefix  = "ALBlogs/${var.app}/ext"
#     enabled = true
#   }
#   lifecycle {
#     create_before_destroy = true
#   }
# }


# ALB Listner on 443
# resource "aws_lb_listener" "alb_listener" {
#   load_balancer_arn = aws_lb.alb.arn
#   port              = "443"
#   protocol          = "HTTPS"
#   ssl_policy        = "ELBSecurityPolicy-FS-2018-06"
#   certificate_arn   = data.aws_acm_certificate.soa-cert.arn
#   default_action {
#     target_group_arn = aws_lb_target_group.alb_target_group.arn
#     type             = "forward"
#   }
#   lifecycle {
#     create_before_destroy = true
#   }
# }
# ALB Listner from 80 to 443
# resource "aws_lb_listener" "alb_listener2" {
#   load_balancer_arn = aws_lb.alb.arn
#   port              = "80"
#   protocol          = "HTTP"

#   default_action {
#     type = "redirect"

#     redirect {
#       port        = "443"
#       protocol    = "HTTPS"
#       status_code = "HTTP_301"
#     }
#   }

#   lifecycle {
#     create_before_destroy = true
#   }
# }




# ALB Target Groups
resource "aws_lb_target_group" "alb_target_group" {
  name     = "tg-ue1-${var.tag}-${var.app}-${var.env}"
  port     = "8080"
  protocol = "HTTP"
  vpc_id   = var.vpc_id
  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 2
    interval            = 10
    protocol            = "HTTP"
    path                = var.path
    port                = 8080
    matcher             = "200"
  }

  stickiness {
    type            = "lb_cookie"
    cookie_duration = 86400
    enabled         = true
  }
  lifecycle {
    create_before_destroy = true
  }
}


resource "aws_lb_target_group_attachment" "this" {
  count            = var.instance_count
  target_group_arn = aws_lb_target_group.alb_target_group.arn
  target_id        = element(split(",", join(",", aws_instance.app.*.id)), count.index)
}