data "template_file" "init-script" {
  template = file("../scripts/${var.app}/init.tpl")
  vars = {
    appname2 = var.app
    env2    = var.env
    DEVICE = var.device
    MOUNT_POINT = var.mount_point
  }
}

data "template_file" "cw_data" {
  template = file("../scripts/${var.app}/cwlog.tpl")

  vars = {
    app   = var.app
  }
}

# data "template_file" "amazon_linux_cloud_init_part" {
#   template = <<EOL
# #!/bin/bash
# # Host entry
# cloud-init-per once host_entry1 echo -e '$${efs_ip_address1} $${efs_dns}' >> /etc/hosts
# cloud-init-per once host_entry2 echo -e '$${efs_ip_address2} $${efs_dns}' >> /etc/hosts
# # Install nfs-utils
# cloud-init-per once yum_update yum update -y
# cloud-init-per once install_nfs_utils yum install -y nfs-utils
# # Create $${mount_location} folder
# cloud-init-per once mkdir_efs mkdir -p $${mount_location}




# # Mount $${mount_location}
# cloud-init-per once mount_efs echo -e '$${efs_dns}:/ $${mount_location} nfs4 defaults,_netdev 0 0' >> /etc/fstab
# mount -a
# EOL

#   vars = {
#     efs_ip_address1 = element(aws_efs_mount_target.efs_mount_target.*.ip_address, 0)
#     efs_ip_address2 = element(aws_efs_mount_target.efs_mount_target.*.ip_address, 1)
#     efs_dns        = element(aws_efs_mount_target.efs_mount_target.*.dns_name, 0)
#     mount_location = var.mount_location
#   }
# }


data "template_cloudinit_config" "cloudinit-example" {
  gzip          = false
  base64_encode = false

  part {
    content_type = "text/x-shellscript"
    content      = data.template_file.init-script.rendered
  }
  # part {
  #   content_type = "text/x-shellscript"
  #   content      = file("../scripts/${var.app}/mnt_data.sh")
  # }
  part {
    content_type = "text/x-shellscript"
    content      = data.template_file.cw_data.rendered
  }
  part {
    content_type = "text/x-shellscript"
    content      = file("../scripts/${var.app}/bootstrap.sh")
  }
  # part {
  #   content_type = "text/cloud-boothook"
  #   content      = data.template_file.amazon_linux_cloud_init_part.rendered
  # }

}



