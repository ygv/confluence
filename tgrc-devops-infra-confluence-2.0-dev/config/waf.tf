resource "aws_wafregional_web_acl" "wafacl" {
  name        = "${var.app}_WebACL"
  metric_name = "jiraWebACL"

  default_action {
    type = "BLOCK"
  }

  rule {
    action {
      type = "ALLOW"
    }

    priority = 1
    rule_id  = data.aws_wafregional_rule.security_team.id
    type     = "REGULAR"
  }

  rule {
    action {
      type = "ALLOW"
    }

    priority = 2
    rule_id  = data.aws_wafregional_rule.managedIPs_wafrule.id
    type     = "REGULAR"
  }
}


# resource "aws_wafregional_web_acl_association" "wafassociation" {
#   depends_on   = [aws_wafregional_web_acl.wafacl]
#   web_acl_id   = aws_wafregional_web_acl.wafacl.id
#   resource_arn = aws_lb.alb.arn
# }


