provider "aws" {
  region = "us-east-1"
  assume_role {
    role_arn     = local.role_arns[var.env]
    session_name = var.env
    external_id  = local.external_id[var.env]
  }
}

locals {
  role_arns = {
    "dev"    = "arn:aws:iam::248607199015:role/tgrc-forge-sharedservices-terraform-role"
    "stage"  = "arn:aws:iam::229101885355:role/tgrc-forge-sharedservices-terraform-role"
    "prod"   = "arn:aws:iam::360297428551:role/tgrc-forge-sharedservices-terraform-role"
    "shared" = "arn:aws:iam::915180751011:role/tgrc-forge-sharedservices-terraform-role"
  }
  external_id = {
    "dev"    = "r8qNRN7kQG3CwEun2UHbRsMJj5B73SNbCdnkqMt2"
    "stage"  = "EllJN92NZB6Yu2bCORx2Mdqe0X4d7k"
    "prod"   = "xDhussA6BJ3YNdPukzod32V7S8c0ys"
    "shared" = "UwvakuVCEv8u5k4CS8dW2WJr"
  }
}

terraform {
  required_version = ">=0.12.6, < 0.14"
  required_providers {
    random = {
      source = "hashicorp/random"
      version = ">= 2.3.0"
    }
    null = {
      source = "hashicorp/null"
      version = "~> 2.1"
    }
    template = {
      source = "hashicorp/template"
      version = "~> 2.1"
    }
    aws = {
      source = "hashicorp/aws"
      version = ">= 2.7.0"
    }
  }
}


terraform {
  backend "s3" {
    bucket               = "tgrc-devops-tf-statefiles"
    //    dynamodb_table       = "tgrc-devops-statefiles-lock"
    acl                  = "bucket-owner-full-control"
    key                  = "terraform.tfstate"
    workspace_key_prefix = "workspaces/tgrc-devops/atlassian-jira"
    region               = "us-east-1"
    encrypt              = true
  }
}