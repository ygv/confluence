# ######## Security Group for ALB

# # resource "aws_security_group" "alb_sg" {
# #   name        = "alb-sg-${var.app}"
# #   description = "Allow inbound traffic and outbound traffic"
# #   vpc_id      = var.vpc_id
# #   tags        = merge(local.tags, map("Name", "alb-sg-${var.app}"))
# # }

# # resource "aws_security_group_rule" "egress" {
# #   type              = "egress"
# #   from_port         = 0
# #   to_port           = 0
# #   protocol          = "-1"
# #   cidr_blocks       = ["0.0.0.0/0"]
# #   security_group_id = aws_security_group.alb_sg.id
# # }
# #### Security Group for APP
# resource "aws_security_group" "app_sg" {
#   name        = "app-sg-${var.app}"
#   description = "Allow inbound traffic and outbound traffic"
#   vpc_id      = var.vpc_id
#   tags        = merge(local.tags, map("Name", "app-sg-${var.app}"))
# }
# resource "aws_security_group_rule" "ingress_app_sg" {
#   type                     = "ingress"
#   protocol                 = "tcp"
#   from_port                = "8080"
#   to_port                  = "8080"
#   security_group_id        = aws_security_group.app_sg.id
#   source_security_group_id = var.alb_sg_id
# }
# resource "aws_security_group_rule" "ingress_app_sg1" {
#   type              = "ingress"
#   protocol          = "tcp"
#   from_port         = "40001"
#   to_port           = "40001"
#   security_group_id = aws_security_group.app_sg.id
#   self              = true
# }
# resource "aws_security_group_rule" "ingress_app_sg2" {
#   type              = "ingress"
#   protocol          = "tcp"
#   from_port         = "80"
#   to_port           = "80"
#   security_group_id = aws_security_group.app_sg.id
#   cidr_blocks       = ["10.0.0.0/8"]
# }

# resource "aws_security_group_rule" "egress_app_sg" {
#   type              = "egress"
#   from_port         = 0
#   to_port           = 0
#   protocol          = "-1"
#   cidr_blocks       = ["0.0.0.0/0"]
#   security_group_id = aws_security_group.app_sg.id
# }


# ######################EFS SG##################
# resource "aws_security_group" "efs_sg" {
#   name        = "efs-sg-${var.app}"
#   description = "controls access to efs"
#   vpc_id      = var.vpc_id
#   lifecycle {
    # create_before_destroy = true
#   }
#   tags = merge(local.tags, map("Name", "efs-sg-${var.app}"))
# }

# resource "aws_security_group_rule" "ingress_efs" {
#   type                     = "ingress"
#   protocol                 = "tcp"
#   from_port                = "2049"
#   to_port                  = "2049"
#   security_group_id        = aws_security_group.efs_sg.id
#   source_security_group_id = aws_security_group.app_sg.id
# }
# resource "aws_security_group_rule" "egress_efs" {
#   type              = "egress"
#   from_port         = 0
#   to_port           = 0
#   protocol          = "-1"
#   cidr_blocks       = ["0.0.0.0/0"]
#   security_group_id = aws_security_group.efs_sg.id
# }