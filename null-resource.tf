provider "aws" {
    region = "us-east-1"
    assume_role {
        role_arn = "arn:aws:iam::161829588545:role/ec2-admin-access"
    }
}

resource "aws_instance" "example" {
  count = 4
  ami = "ami-01cc34ab2709337aa"
  instance_type = "t2.micro"
  vpc_security_group_ids = ["sg-00272b3637ef0ccd3"]
  associate_public_ip_address = true
  key_name = "test-key"
  tags = {
          Name = "terraform-example${count.index + 1}"
         }
}

#################################

resource "null_resource" "configure" {

        count = "${length(aws_instance.example.*.ami)}"

        connection {
        user        = "ec2-user"
        type        = "ssh"
        host        = "${element(aws_instance.example.*.private_ip, count.index)}"
        private_key = "${file("test-key.pem")}"
        }

    provisioner "remote-exec" {
        inline = [
            "sudo yum remove awscli -y"
        ]
    }

}